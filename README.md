# TripperBay #


### Quick start the development environment

Clone the projects:

    git clone git@bitbucket.org:tripperbay/tripperbay.git
    cd tripperbay
    git clone git@bitbucket.org:tripperbay/frontend.git
    git clone git@bitbucket.org:tripperbay/backend.git

Start pgsql and redis:

    cd ../
    docker-compose -f docker-compose-local-dbs.yml up

Install backend and add a local.py settings file:

    cd backend
    pip install -r requirements.txt
    cp tripperbay/settings/local_example.py tripperbay/settings/local.py

On the very first start, migrate the DB, collect static and load fixtures:

    export $(cat example.env | xargs)
    ./manage.py migrate --noinput
    ./manage.py collectstatic --noinput
    ./manage.py loaddata fixtures/currencies.json
    ./manage.py loaddata fixtures/geo.json
    ./manage.py loaddata fixtures/interests.json
    ./manage.py loaddata fixtures/sites.json
    ./manage.py loaddata fixtures/socialapps.json
    ./manage.py loaddata fixtures/unsplash.json

Start backend:

    export $(cat example.env | xargs) && ./manage.py runserver

Install frontend and start it up:

    cd ../frontend
    npm install
    export $(cat example.env | xargs) && npm run dev


### Deploy

#### Locally

We use docker to simplify the deployments. To get started, run the following:

    docker-compose down
    docker-compose up --build -d

For the very first builds we need to migrate the database, collect static files and load some fixtures.

    ./scripts/migrate_db.sh
    ./scripts/collect_static.sh
    ./scripts/load_fixtures.sh

All of the above will create the necessary docker containers __on the local machine__ and load required data so that the app will run.

Add line `127.0.0.1 tripperbay.com www.tripperbay.com` to your `/etc/hosts` file

#### Remotely

Use [docker-machine](https://docs.docker.com/machine/) for remote deploys

    docker-machine create --driver generic --generic-ip-address=IP_ADDR --generic-ssh-key ~/.ssh/id_rsa --generic-ssh-user=USER_WITH_SUDO_RIGHTS MACHINE_NAME

Remove line `127.0.0.1 tripperbay.com www.tripperbay.com` from your `/etc/hosts` file

### Misc

#### Deploy services individually

To deploy just one service, eg. __frontend__ run the following:

    docker-compose build frontend
    docker-compose up --no-deps -d frontend
