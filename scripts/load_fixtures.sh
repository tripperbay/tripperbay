echo "Loading currencies ..."
docker cp backend/fixtures/currencies.json tb-backend:/tripperbay/backend/
docker exec -it tb-backend python manage.py loaddata currencies.json

echo "Loading geo. Hold your horses, this might take a while ..."
docker cp backend/fixtures/geo.json tb-backend:/tripperbay/backend/
docker exec -it tb-backend python manage.py loaddata geo.json

echo "Loading interests ..."
docker cp backend/fixtures/interests.json tb-backend:/tripperbay/backend/
docker exec -it tb-backend python manage.py loaddata interests.json

echo "Loading sites ..."
docker cp backend/fixtures/sites.json tb-backend:/tripperbay/backend/
docker exec -it tb-backend python manage.py loaddata sites.json

echo "Loading socialapps ..."
docker cp backend/fixtures/socialapps.json tb-backend:/tripperbay/backend/
docker exec -it tb-backend python manage.py loaddata socialapps.json

echo "Loading unsplash ..."
docker cp backend/fixtures/unsplash.json tb-backend:/tripperbay/backend/
docker exec -it tb-backend python manage.py loaddata unsplash.json
