echo "Collecting static ..."
docker exec -it tb-backend python manage.py collectstatic --noinput

echo "Collecting custom static ..."
docker cp media/static/img/ tb-backend:/tripperbay/media/static
docker exec -it tb-backend chown -R root. /tripperbay/media
